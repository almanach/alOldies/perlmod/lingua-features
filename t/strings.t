#!/usr/bin/perl
# $Id$

use Lingua::Features;
use Test::More;
use strict;

# compute plan
open(STRINGS, 'strings') or die "unable to open strings: $!";
my @strings = <STRINGS>;
close(STRINGS);
plan tests => scalar @strings;

foreach my $string (@strings) {
    chomp $string;
    is(
	Lingua::Features::Structure->from_string($string)->to_string(),
	$string,
	"$string parsing"
    );
}
